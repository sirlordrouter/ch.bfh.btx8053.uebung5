package gen;

import java.io.FileOutputStream;
import java.util.List;
import java.util.Map.Entry;

import javax.xml.stream.XMLEventFactory;
import javax.xml.stream.XMLEventWriter;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.events.XMLEvent;

import definitions.Case;
import definitions.Staff;
import definitions.Treatment;

public class StaxWriter2 implements IXmlWriter {

	@Override
	public void writeXML(String filepath, List<Case> c) {
		// TODO Auto-generated method stub

		try {
		XMLEventWriter xmlWriter = 
				XMLOutputFactory.newInstance(). createXMLEventWriter(
						new FileOutputStream(filepath));
			XMLEventFactory exFactory = XMLEventFactory.newInstance();
			XMLEvent startDocument = exFactory.createStartDocument(); 
			XMLEvent endDocument = exFactory.createEndDocument();
			XMLEvent startCases = exFactory.createStartElement("",	"", "cases");
			XMLEvent endCases = exFactory.createEndElement("",	"", "cases");
			
			xmlWriter.add(startDocument); 
			xmlWriter.add(startCases);

			for (Case ca : c) {
				//case
				XMLEvent startCase = exFactory.createStartElement("", "", "case"); 
				XMLEvent endCase = exFactory.createEndElement("", "", "case" ); 
				//Patient
				XMLEvent startPatient = exFactory.createStartElement("", "", "name"); 
				XMLEvent endPatient = exFactory.createEndElement("", "", "name" ); 
				XMLEvent pName = exFactory.createCharacters(ca.getPatient());
				//Datum
				XMLEvent scDatum = exFactory.createStartElement("", "", "Datum"); 
				XMLEvent ecDatum = exFactory.createEndElement("", "", "Datum" ); 
				XMLEvent daName = exFactory.createCharacters(ca.getDatum().toString());
				//Diagnose
				XMLEvent scDiag = exFactory.createStartElement("", "", "Diagnose"); 
				XMLEvent ecDiag = exFactory.createEndElement("", "", "DIagnose" ); 
				XMLEvent dName = exFactory.createCharacters(ca.getDiagnosis());
				
				XMLEvent startTreatments = exFactory.createStartElement("", "", "treatments"); 
				XMLEvent endTreatments = exFactory.createEndElement("", "", "treatments" ); 
				
				xmlWriter.add(startCase);
				xmlWriter.add(startPatient);
				xmlWriter.add(pName);
				xmlWriter.add(endPatient);
				xmlWriter.add(scDatum);
				xmlWriter.add(daName);
				xmlWriter.add(ecDatum);
				xmlWriter.add(scDiag);
				xmlWriter.add(dName);
				xmlWriter.add(ecDiag);

				xmlWriter.add(startTreatments);
				for (Treatment t : ca.getTreatments()) {
					XMLEvent startTreatment = exFactory.createStartElement("", "", "treatment"); 
					XMLEvent endTreatment = exFactory.createEndElement("", "", "treatment" ); 
					XMLEvent startName = exFactory.createStartElement("", "", "name"); 
					XMLEvent endName = exFactory.createEndElement("", "", "name" ); 
					XMLEvent tName = exFactory.createCharacters(t.getName());
					XMLEvent sstDatum = exFactory.createStartElement("", "", "Datum"); 
					XMLEvent estDatum = exFactory.createEndElement("", "", "Datum" ); 
					XMLEvent tDatum = exFactory.createCharacters(t.getDatum().toString());
					XMLEvent sstStart = exFactory.createStartElement("", "", "Start"); 
					XMLEvent estStart = exFactory.createEndElement("", "", "Start" ); 
					XMLEvent tStart = exFactory.createCharacters(t.getStartZeit().toString());
					XMLEvent sstEnde = exFactory.createStartElement("", "", "Ende"); 
					XMLEvent estEnde = exFactory.createEndElement("", "", "Ende" ); 
					XMLEvent tEnde = exFactory.createCharacters(t.getEndZeit().toString());
					
					xmlWriter.add(startTreatment);
					
					xmlWriter.add(startName);
					xmlWriter.add(tName);
					xmlWriter.add(endName);
					xmlWriter.add(sstDatum);
					xmlWriter.add(tDatum);
					xmlWriter.add(estDatum);
					xmlWriter.add(sstStart);
					xmlWriter.add(tStart);
					xmlWriter.add(estStart);
					xmlWriter.add(sstEnde);
					xmlWriter.add(tEnde);
					xmlWriter.add(estEnde);
					
					for (Staff s : t.getStaff()) {
						XMLEvent startStaff = exFactory.createStartElement("", "", "staff"); 
						XMLEvent endStaff = exFactory.createEndElement("", "", "staff" ); 
						XMLEvent name = exFactory.createCharacters(s.getForename() + " " + s.getName());
						xmlWriter.add(startStaff); 
						xmlWriter.add(name); 
						xmlWriter.add(endStaff); 
					}
					xmlWriter.add(endTreatment);
					
				}
				xmlWriter.add(endTreatments);
				xmlWriter.add(endCase);
			}
			xmlWriter.add(endCases);
			xmlWriter.add(endDocument);
			xmlWriter.close();
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}

}
