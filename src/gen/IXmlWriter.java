package gen;


import java.util.List;

import definitions.Case;

public interface IXmlWriter {

	public void writeXML(String filepath, List<Case> c);
}
