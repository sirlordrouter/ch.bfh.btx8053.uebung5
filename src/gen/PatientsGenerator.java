package gen;

import java.util.ArrayList;
import java.util.List;

import definitions.Case;
import definitions.Date;
import definitions.Staff;
import definitions.Time;
import definitions.Treatment;

public class PatientsGenerator {

	private String[] forenames = {"Hans", "Moritz", "Fritz", "Gregor", "Anna", "Vreni", "Ursula"};
	private String[] names = {"Mustermann", "Meier", "Frankenstein", "Toller", "Muderig", "Heiler", "Grunder"};
	private String[] diagnosis = {"Broken Leg", "Diahrroe", "Bronchitis", "Herzinfarkt", "Rückenschmerzen"};
	private String[] treatments = {"Bibaepele", "Chuederle", "Fueeuschmi-Gspuerschmi", "Chli Chueele", "Naeie das Zueg"};

	public PatientsGenerator() {
		
	}
	
	private int randomInt (int min, int max) {
		return (int) Math.round(Math.random() * (max-min) + min);
	}
	
	private String randomString(String[] s) {
		return s[randomInt(0, s.length-1)];
	}
	
	private Date createRandomDate() {
		Date d = new Date(
				randomInt(1, 28),
				randomInt(1, 12),
				randomInt(2000, 2013));
		return d; 
	}
	
	private Time createRandomTime () {
		Time t = new Time(
				randomInt(0, 23),
				randomInt(0, 59),
				randomInt(0, 59));
		return t; 
	}
	
	public List<Case> createRandomCases(int caseCount, int maxTreatmentCount, int maxStaffCount) {
		List<Case> cs = new ArrayList<Case>();
		for (int i = 0; i < caseCount; i++) {
			Case c = new Case(
					randomString(forenames) + " " + randomString(names), 
					createRandomDate(), 
					randomString(diagnosis), 
					createRandomTreatments(randomInt(1, maxTreatmentCount), 
							createRandomStaff(randomInt(1, maxStaffCount))));
			cs.add(c);
		}
		return cs; 
	}
	
	private List<Staff> createRandomStaff(int staffCount) {
		List<Staff> s = new ArrayList<Staff>();
		for (int i = 0; i < staffCount; i++) {
			s.add(new Staff(randomString(forenames), randomString(names)));
		}
		return s;
	}

	private List<Treatment> createRandomTreatments(int count, List<Staff> staff) {
		List<Treatment> ts = new ArrayList<Treatment>();
		for (int i = 0; i < count ; i++) {
			Treatment t = new Treatment(createRandomDate(), 
					createRandomTime(), createRandomTime(),
					randomString(treatments), staff);
			ts.add(t);
		}
		return ts;
	}
}
