package definitions;


import java.util.List;

public class Treatment {

	private String name;
	private Date datum;
	private Time startZeit;
	private Time endZeit;
	private List<Staff> staff;
	
	public Treatment(Date d, Time s, Time e,String treatmentName, List<Staff> st) {
		datum = d;
		startZeit = s;
		endZeit = e;
		staff = st; 
		setName(treatmentName);
	}

	public Date getDatum() {
		return datum;
	}

	public void setDatum(Date datum) {
		this.datum = datum;
	}

	public Time getStartZeit() {
		return startZeit;
	}

	public void setStartZeit(Time startZeit) {
		this.startZeit = startZeit;
	}

	public Time getEndZeit() {
		return endZeit;
	}

	public void setEndZeit(Time endZeit) {
		this.endZeit = endZeit;
	}

	public List<Staff> getStaff() {
		return staff;
	}

	public void setStaff(List<Staff> staff) {
		this.staff = staff;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	
}
