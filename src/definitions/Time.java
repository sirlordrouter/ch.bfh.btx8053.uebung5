package definitions;


public class Time {

	private int hour;
	private int minute;
	private int second;
	
	public Time(int h, int m, int s) {
		hour = h;
		minute = m;
		second = s;
	}

	public int getHour() {
		return hour;
	}

	public void setHour(int hour) {
		this.hour = hour;
	}

	public int getMinute() {
		return minute;
	}

	public void setMinute(int minute) {
		this.minute = minute;
	}

	public int getSecond() {
		return second;
	}

	public void setSecond(int second) {
		this.second = second;
	}
	
	public String getTime() {
		return hour + ":" + minute +":" + second; 
	}
	
	@Override
	public String toString() {
		return hour + ":" + minute +":" + second; 
	}

}
