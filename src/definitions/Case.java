package definitions;


import java.util.List;

public class Case {
	
	private String patient; 
	private Date datum; 
	private String diagnosis; 
	private List<Treatment> treatments; 

	public Case(String p, Date d, String dia, List<Treatment> t) {
		patient = p;
		datum = d; 
		diagnosis = dia;
		treatments=t;
	}

	public String getPatient() {
		return patient;
	}

	public void setPatient(String patient) {
		this.patient = patient;
	}

	public Date getDatum() {
		return datum;
	}

	public void setDatum(Date datum) {
		this.datum = datum;
	}

	public String getDiagnosis() {
		return diagnosis;
	}

	public void setDiagnosis(String diagnosis) {
		this.diagnosis = diagnosis;
	}

	public List<Treatment> getTreatments() {
		return treatments;
	}

	public void setTreatments(List<Treatment> treatments) {
		this.treatments = treatments;
	}
}
